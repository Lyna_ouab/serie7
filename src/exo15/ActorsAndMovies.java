package exo15;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;


public class ActorsAndMovies {

    public static void main(String[] args) {

        ActorsAndMovies actorsAndMovies = new ActorsAndMovies();
        Set<Movie> movies = actorsAndMovies.readMovies();
        
        //Nombre de film
        System.out.println("movies.size()) = " + movies.size());
        
        //Nombre d'acteurs:
        Long numberOfActors = movies.stream().flatMap(m -> m.actors().stream())
        		.distinct()
        		.count();
        System.out.println("\nNumeber of actors = " + numberOfActors );
        
        //Nombre d'ann�es:
        Long numberOfYears = movies.stream().map(m -> m.releaseYear())
        		.distinct()
        		.count();
        System.out.println("\nNumeber of years = " + numberOfYears );
        
        //Le film le plus vieux et le plus recent 
        int yearOfOldestMovie = movies.stream().mapToInt(m -> m.releaseYear())
        		.min().orElseThrow();
        int yearOfRecentMovie = movies.stream().mapToInt(m -> m.releaseYear())
        		.max().orElseThrow();
        System.out.println("\nThe oldest movie was released in " + yearOfOldestMovie
        		+ " and the most recent one in " + yearOfRecentMovie);
        
        //Le max de film sorti en une ann�e
        Map<Integer, Long> moviesByYears = movies.stream()
        		.collect
        			(Collectors.groupingBy(
        					m -> m.releaseYear(),
        					Collectors.counting()
        					));
        Entry<Integer, Long> maxMoviesInAYear = moviesByYears.entrySet().stream()
        		.max(Comparator.comparing(Entry::getValue))
        		.orElseThrow();
        System.out.println("\nThe most productive year was " + maxMoviesInAYear.getKey() 
        		+ " with " + maxMoviesInAYear.getValue() + " movies released");
        
        //Le film avec le plus grand nombre d'acteurs
        Movie movieWithMaxActors =
        		movies.stream()
        		.max(Comparator
        				.comparing(movie-> movie.actors().size())
        			)
        		.orElseThrow();
        		
        System.out.println("\nThe movie with the bigest casting is " + 
        		movieWithMaxActors.title() + " with " + movieWithMaxActors.
        		actors().size() + " actors");
        
        //L'acteur qui a jou� dans le max de films
         Map<Actor, Long> ActorsMap = 
		        	movies.stream() //Map<Actor, Long>
		        		.flatMap(m -> m.actors().stream())
		        		.collect(
		        				Collectors.groupingBy(
		        						Function.identity(),
		        						Collectors.counting()
		        						)
		        				);
         Entry<Actor, Long> ActorWithMaxMovies = 
        		 ActorsMap.entrySet().stream()
	         		.max(Comparator.comparing(Entry::getValue))
	         		.orElseThrow();
        System.out.println("\n" + ActorWithMaxMovies.getKey().firstName +" " 
         		+ ActorWithMaxMovies.getKey().lastName + " is the actor who appears most "
         		+ "often. \n\tHe starred in " + ActorWithMaxMovies.getValue() + " movies");
          
        //L'acteur qui a jou� dans le max de films en une seule ann�e
        Collector<Movie, Integer, Entry<Actor, Long>> collector = 
        		Collectors.collectingAndThen(
						Collectors.flatMapping(
								m -> m.actors().stream(), 
				 				Collectors.groupingBy(
				 						Function.identity(),
				 						Collectors.counting()
				 				)
				 		),
						map -> map.entrySet().stream()
				     		.max(Comparator.comparing(Entry::getValue))
				     		.orElseThrow()
        		);
        
        Map<Integer, Entry<Actor, Long>> mapWithBigestActorPerYear = 
        		movies.stream()
		         	  .collect(
		         			Collectors.groupingBy(
		         					Movie::releaseYear,
		         					collector
		         			)
		         	  );
        Entry <Integer, Entry<Actor, Long>> ActorWithMaxMovies2 =
    		  mapWithBigestActorPerYear.entrySet().stream()
		    		  	.max(Comparator.comparing(e -> e.getValue().getValue()))
		    		  	.orElseThrow();
		System.out.println("By Using a Collector");
		System.out.println( ActorWithMaxMovies2.getValue().getKey().firstName + " "
      		+ ActorWithMaxMovies2.getValue().getKey().lastName + " is the actor who shot the"
      		+ " most movies in a year.\n\tHe played in " + ActorWithMaxMovies2.getValue()
      		.getValue() + " movies in " + ActorWithMaxMovies2.getKey());
        
      
		//les deux acteurs qui ont le plus jou� ensemble
		//a
		Comparator<Actor> compareByLastName =
				  Comparator.comparing(t -> t.lastName());
		Comparator<Actor> compareByfirstName =
				  Comparator.comparing(Actor::firstName);
		  
		Comparator<Actor> comparatorOfActors =
		  compareByLastName.thenComparing(compareByfirstName);
      
//      //b
//      BiFunction< Stream<Actor>, Actor, Stream<Map.Entry<Actor, Actor>>>
//      	entryOfActorsFromSteam = (streamOfActors,actor) -> streamOfActors
//    		  	.filter(secondActor -> 
//    		  				comparatorOfActors.compare(secondActor,actor) != 0)
//    		  	.map(secondActor -> Map.entry(secondActor,actor));
      	
  		BiFunction< Stream<Actor>, Actor, Stream<Map.Entry<Actor, Actor>>>
      	streamOfEntryOfActorsFromSteam = (streamOfActors,actor) -> streamOfActors
	      	.filter(secondActor -> 
	      		comparatorOfActors.compare(secondActor,actor) < 0)
	      	.map(secondActor -> Map.entry(secondActor,actor));
    		  	//test
//		Actor d = new Actor("D","d");
//		List<Actor> actors = List.of(
//				new Actor("A","a"),
//				new Actor("B","b"),
//				new Actor("D","d"),
//				new Actor("C","c")
//				);
//      Stream<Map.Entry<Actor, Actor>>	test =  
//      				distinctEntryOfActorsFromSteam.apply(actors.stream(),d);
//      test.forEach(t -> System.out.println( "(" + t.getKey() + ", " + t.getValue() + ")"));

    	//c
    	Function<Movie, Stream<Actor>> streamOfActrorsFromMovie =
    		  movie -> movie.actors().stream();
			    
    	//d    			
		BiFunction<Movie, Actor, Stream<Map.Entry<Actor, Actor>>> 
				streamOfEntryOfActrorsFromMovie = (movie , actor) ->
					streamOfEntryOfActorsFromSteam
								.apply(streamOfActrorsFromMovie.apply(movie),actor);
    	//e    				
		Function<Movie, Stream<Map.Entry<Actor, Actor>>> streamOfPairsOfActorsInAMovie =
				movie ->  
    				streamOfActrorsFromMovie.apply(movie)
    					.flatMap
    						(actor -> streamOfEntryOfActrorsFromMovie.apply(movie,actor))
    					.distinct();
		//f
//    	long numberOfAllPairsOfActors = movies.stream()
//    			.flatMap(movie -> pairsOfActors.apply(movie))
//    			.distinct()
//    			.count();
//    	System.out.println("Number of couples of actors:\t" + numberOfAllPairsOfActors);
    				
		Supplier <Stream<Entry<Actor, Actor>>> streamOfPairsOfActors = 
				() -> movies.stream()
				.flatMap(movie -> streamOfPairsOfActorsInAMovie.apply(movie));
    				
		long numberOfPairsOfActors = streamOfPairsOfActors.get()
				.count();
		System.out.println("Number of couples of actors:\t" + numberOfPairsOfActors);
    	
    	long numberOfDistinctPairsOfActors = streamOfPairsOfActors.get()
    			.distinct()
    			.count();
    	System.out.println("Distinct number of couples of actors:\t" 
    						+ numberOfDistinctPairsOfActors);
    	
    	//g
    	//Map<Entry<Actor, Actor>, Long> 
    	Entry<Entry<Actor, Actor>, Long> thePairOfActors = streamOfPairsOfActors.get()
    						.collect(
    									Collectors.groupingBy(
    											Function.identity(),
    											Collectors.counting()
    									)
    								).entrySet().stream()
							 		.max(Comparator.comparing(Entry::getValue))
							 		.orElseThrow();
    	System.out.println(thePairOfActors.getKey().getKey() + " and " + thePairOfActors
    			.getKey().getValue() + " are the actors who have worked the most together,They did it "
    			+ thePairOfActors.getValue() + " times.");
    	
    	//10
        Collector<Movie, Integer, Entry<Entry<Actor, Actor>, Long>> collectorOfPair = 
        		Collectors.collectingAndThen(
        				//downstream //Collector
						Collectors.flatMapping(	
								movie -> streamOfPairsOfActorsInAMovie.apply(movie),
												Collectors.groupingBy(
														Function.identity(),
														Collectors.counting()
												)
						),
						//finisher //Function
						map -> map.entrySet().stream()
								.max(Comparator.comparing(Entry::getValue))
								.orElseThrow()
        		);
    	
        Entry<Integer, Entry<Entry<Actor, Actor>, Long>> thePairOfActorsPerYear = 
        		movies.stream()
		         	  .collect(
		         			Collectors.groupingBy(
		         					Movie::releaseYear,
		         					collectorOfPair
		         			)
		         	  ).entrySet().stream()
					  	.max(Comparator.comparing(e -> e.getValue().getValue()))
					  	.orElseThrow();
				    	
        
        System.out.println(thePairOfActorsPerYear.getValue().getKey().getValue() + " and " 
        		+ thePairOfActorsPerYear.getValue().getKey().getKey() + " are the actors who"
        		+ " have worked the most together in a sigle year,\nThey did it "
        		+ thePairOfActorsPerYear.getValue().getValue() + " times in " 
        		+ thePairOfActorsPerYear.getKey());
    	
    }

    public Set<Movie> readMovies() {

        Function<String, Stream<Movie>> toMovie =
                line -> {
                    String[] elements = line.split("/");
                    String title = elements[0].substring(0, elements[0].lastIndexOf("(")).trim();
                    String releaseYear = elements[0].substring(elements[0].lastIndexOf("(") + 1, elements[0].lastIndexOf(")"));
                    if (releaseYear.contains(",")) {
                    	int indexOfComa = releaseYear.indexOf(",");
                    	releaseYear = releaseYear.substring(0,indexOfComa);
                    }
                    Movie movie = new Movie(title, Integer.valueOf(releaseYear));


                    for (int i = 1; i < elements.length; i++) {
                        String[] name = elements[i].split(", ");
                        String lastName = name[0].trim();
                        String firstName = "";
                        if (name.length > 1) {
                            firstName = name[1].trim();
                        }

                        Actor actor = new Actor(lastName, firstName);
                        movie.addActor(actor);
                    }
                    return Stream.of(movie);
                };

        try (FileInputStream fis = new FileInputStream("files/movies-mpaa.txt.gz");
             GZIPInputStream gzis = new GZIPInputStream(fis);
             InputStreamReader reader = new InputStreamReader(gzis);
             BufferedReader bufferedReader = new BufferedReader(reader);
             Stream<String> lines = bufferedReader.lines();
        ) {

            return lines.flatMap(toMovie).collect(Collectors.toSet());

        } catch (IOException e) {
            System.out.println("e.getMessage() = " + e.getMessage());
        }

        return Set.of();
    }
}
